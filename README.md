# stateScript #

###Microcontroller firmware that uses a simple, yet powerful scripting language to control the timing of input and output events with high temporal resolution. Written specifically for neuroscience research. ###

-------



### Uploading firmware to your controller ###

stateScript is currently supported on the SpikeGadgets hardware and on the MBED LPC1768.  Support for other hardware is planned.

To compile (or contribute) for MBED LPC1768, go [here](https://developer.mbed.org/users/mkarlsso/code/stateScript_v2/) and branch the project.  You can also download the .bin file in the downloads section of this repository and simply copy it to your MBED's flash to get going.

stateScript comes pre-loaded on the SpikeGadgets Environmental Control Unit (ECU).

### Graphical Interface ###

A graphical interface to interact with the firmware and edit scripts is part of the Trodes Project.  Trodes can be downloaded for windows [here](https://bitbucket.org/mkarlsso/trodes/downloads). Double-click on 'stateScript' to start the GUI. Here is a screenshot of the program when it has just opened:

|



![Screen Shot 2015-07-01 at 4.55.44 PM.png](https://bitbucket.org/repo/BpeM75/images/2603986754-Screen%20Shot%202015-07-01%20at%204.55.44%20PM.png)

Once the program has launched, you will need to connect to the hardware. More to come...

----
### Scripting ###


```
#!matlab

%Example 1
%Example of a timed blink

%define some global variables
int allowblink = 1
int blinkdelay = 1000

% when digital port 1 goes from low to high...
callback portin[1] up
	if (allowblink == 1) do  % if statement
		portout[1] = 1 % set output port 1 to high
		do in blinkdelay % this block will executed in blinkdelay ms
			portout[1] = 0 % set output port 1 to low
		end
	else do  % else block
		disp('No blinking allowed.')
	end
end; % use a semicolon to tell the compiler to compile everything up to this point

```