#include "debugInterface.h"


//--------------------------------------------------------------
//--------------------------------------------------------------
//This section is required for all custom harware interfaces

//globals defined in hardwareInterface.cpp
extern uint32_t timeKeeper; //the master clock
extern bool resetTimer;
extern bool clockSlave;
extern bool changeToSlave;
extern bool changeToStandAlone;
extern outputStream textDisplay;

//FPGA-specific variables
//------------------------------------------
DebugDigitalIn dIn[NUMDIGINPORTS];
DebugDigitalOut dOut[NUMDIGOUTPORTS];
DebugAnalogIn aIn[NUMANINPORTS];
DebugAnalogOut aOut[NUMANOUTPORTS];

//Circular buffer for serial input
uint8_t serialInBuffer[4096];
uint16_t serialInBufferReadLocation = 0;
uint16_t serialInBufferWriteLocation = 0;
uint64_t numSerialInBytesWritten = 0;
uint64_t numSerialInBytesRead = 0;
//--------------------------------------
//------------------------------------------------------------


DebugSystem::DebugSystem()
    //Define the clock reset and increment pins here

{



}

void DebugSystem::timerinit() {



}

void DebugSystem::pauseInterrupts() {

}

void DebugSystem::resumeInterrupts() {

}

int DebugSystem::getPendingFunctionTriggers(uint16_t* bufferPtr) {

    return 0;
}

uint32_t FPGASystem::getDigitalOutputChangeFlags() {

}

uint32_t FPGASystem::getDigitalInputChangeFlags() {

}

//----------------------------------------------

sDigitalOut* DebugSystem::getDigitalOutPtr(int portNum){
    if (portNum < NUMPORTS) {
        return &dOut[portNum];
    } else {
        return NULL;
    }
}

sDigitalIn* DebugSystem::getDigitalInPtr(int portNum) {
    if (portNum < NUMPORTS) {
        return &dIn[portNum];
    } else {
        return NULL;
    }
}

sAnalogOut* DebugSystem::getAnalogOutPtr(int portNum){
    if (portNum < NUMANOUTPORTS) {
        return &aOut[portNum];
    } else {
        return NULL;
    }
}

sAnalogIn* DebugSystem::getAnalogInPtr(int portNum) {
    if (portNum < NUMANINPORTS) {
        return &aIn[portNum];
    } else {
        return NULL;
    }
}

sSound* DebugSystem::createNewSoundAction() {
    DebugSound *tmpSound = new DebugSound();
    return tmpSound;
}


void DebugSystem::externalClockReset() {
    if (timeKeeper > 100) {


        //FPGA-specific code to reset timer here------


        //----------------------------------------------


        immediateClockReset();
    }
}

void DebugSystem::setStandAloneClock() {
    timerinit();

    //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------
    //clockExternalIncrement.rise(NULL); //remove the callback to the external interrupt
    //------------------------------------------------------

    clockSlave = false;
    changeToSlave = false;
    changeToStandAlone = false;
}

void DebugSystem::setSlaveClock() {

    //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------
    //NVIC_DisableIRQ(TIMER0_IRQn); // Disable the timer interrupt
    //clockExternalIncrement.rise(this, &FPGASystem::incrementClock); //enable the external pin interrupt
    //----------------------------------------------------
    clockSlave = true;
    changeToSlave = false;
    changeToStandAlone = false;
}

void DebugSystem::incrementClock() {
    timeKeeper++;
    //Clock resets happen upon update so we dont get a partial first ms
    if (resetTimer) {
        timeKeeper = 0;
        resetTimer = false;
    }
}

void DebugSystem::mainLoopToDo() {

}

//-----------------------------------------------------

DebugSound::DebugSound() {

}

void DebugSound::execute() {
    if (reset) {

    } else if (!play) {
        //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------
        //code to stop playback here
        //sWav.stopTrack();
        //----------------------------------------------
    } else {
        if (volume > -1) {
            //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------
            //Code to change volume
            //sWav.volume(volume);
            //------------------------------------------
        } else if (volumePtr != NULL) {
            //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------
            //code to change volume
            //sWav.volume(*volumePtr);
            //---------------------------------------------
        }

        if (fileNameExists) {
            //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------

            //code to first stop playback, then play the file ("myFile" means there is a "myFile.wav" on the sd card)
            //sWav.stopTrack();
            //sWav.playTrackName(fileName);
            //------------------------------------------------
        }
    }
}

//-----------------------------------------------------
DebugAnalogOut::DebugAnalogOut() {

}

void DebugAnalogOut::init(int pin) {
    pinNumber = pin;

    //---------------------------------------------------
}

int DebugAnalogOut::read() {


    return 0;

}

void DebugAnalogOut::write(int value) {


}
//--------------------------------------------------------

DebugAnalogIn::DebugAnalogIn() {

}

void DebugAnalogIn::init(int pin) {

    pinNumber = pin;


}

int DebugAnalogIn::read() {

    return 0;

}


//-----------------------------------------------------
DebugDigitalOut::DebugDigitalOut() {

}

void DebugDigitalOut::init(int pin) {
    pinNumber = pin;
    currentState = 0;

    //---------------------------------------------------
}

int DebugDigitalOut::read() {

    //read the pin state

    return currentState;

}

void DebugDigitalOut::write(int value) {

    currentState = value;

}
//--------------------------------------------------------

DebugDigitalIn::DebugDigitalIn() {

}

void DebugDigitalIn::init(int pin) {

    //create an input pin object, and an interrupt object for the same pin
    pinNumber = pin;


}

int DebugDigitalIn::read() {

    //code to read to digital input pin
    return 0;

    //----------------------------------------------------
}

void DebugDigitalIn::interrupt_up_callback() {
    addStateChange(1, timeKeeper);
}

void DebugDigitalIn::interrupt_down_callback() {
    addStateChange(0, timeKeeper);
}

//----------------------------------------------------------
DebugSerialPort::FPGASerialPort() {

}

void DebugSerialPort::init() {


    //initialize serial communication for standalone operation (115200 baud)
    //or a method of talking through the MCU in slave mode
    // Init UART0
    //UART0_UBRR = 53; // 115200  baud
    //UART0_UCR = UART_UCR_ENABLE | UART_UCR_RX_IE; // uart enable, uart rx interrupt enable


}

bool DebugSerialPort::readable() {

    //determine whether or not there is anything to read
    //we must disable interrupts for this to be safe
    uint64_t bytesAvailable;

    bytesAvailable = numSerialInBytesWritten-numSerialInBytesRead;


    if (bytesAvailable > 0) {
        return true;
    } else {
        return false;
    }
}

char DebugSerialPort::readChar() {
    //get the next character in the input buffer
    char outchar = serialInBuffer[serialInBufferReadLocation];
    serialInBufferReadLocation = (serialInBufferReadLocation++ % SERIALINPUTBUFFERSIZE);
    numSerialInBytesRead++;

    return outchar;
}

void FPGASerialPort::writeChar(char s) {

    //code to print a single character to the output buffer

}


