#ifndef DEBUGINTERFACE_H
#define DEBUGINTERFACE_H


#include "hardwareInterface.h"
#include <stdint.h>
#include <string.h>
#include <string>
#include <vector>
#include <queue>
#include <stdio.h>
#include "registers.h"
#include "utils.h"
#include "ff.h"
#include "diskio.h"

#define SERIALINPUTBUFFERSIZE 4096


#define NUMPORTS 32 //the number of ports available on this hardware
#define NUMDIGINPORTS 32 //the number of ports available on this hardware
#define NUMDIGOUTPORTS 32
#define NUMANINPORTS 8
#define NUMANOUTPORTS 4

class DebugAnalogOut : public sAnalogOut
{
public:
    DEbugAnalogOut();

    void init(int pin);
    void write(int value);
    int read();

private:
    //define the hardware output pin
    uint8_t pinNumber;

};

class DebugAnalogIn : public sAnalogIn
{
public:
    DebugAnalogIn();

    void init(int pin);
    int read();

protected:

private:
    uint8_t pinNumber;


};

class DebugDigitalOut : public sDigitalOut
{
public:
    DebugDigitalOut();

    void init(int pin);
    void write(int value);
    int read();

private:
    //define the hardware output pin
    uint8_t pinNumber;
    int currentState;

};

class DebugDigitalIn : public sDigitalIn
{
public:
    DebugDigitalIn();

    void init(int pin);
    int read();
    void interrupt_up_callback();
    void interrupt_down_callback();
protected:

private:
    uint8_t pinNumber;
    //define the harware input pin
    //DigitalIn *inpin;
    //InterruptIn *inpin_interrupt;

};

class DebugSerialPort : public sSerialPort
{
public:
    DebugSerialPort();

    void init();
    bool readable();
    char readChar();
    void writeChar(char s);
    //Serial *serialToPC;

private:



};

class DebugSound: public sSound
{
public:
    DebugSound();
    void execute();

private:

};

class DebugSystem: public sSystem
{
public:
    DebugSystem();
    void timerinit();
    void setStandAloneClock();
    void setSlaveClock();
    sDigitalOut* getDigitalOutPtr(int portNum);
    sDigitalIn* getDigitalInPtr(int portNum);
    sAnalogOut* getAnalogOutPtr(int portNum);
    sAnalogIn* getAnalogInPtr(int portNum);
    sSound* createNewSoundAction();
    void incrementClock();
    void externalClockReset(); //needs to reset harware timer before calling immediateClockReset();
    void mainLoopToDo();
    void pauseInterrupts();
    void resumeInterrupts();
    int  getPendingFunctionTriggers(uint16_t *bufferPtr);

protected:

private:

};

#endif // DEBUGINTERFACE_H
