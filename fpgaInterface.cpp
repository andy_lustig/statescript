#include "fpgaInterface.h"

//--------------------------------------------------------------
//--------------------------------------------------------------
//This section is required for all custom harware interfaces

//globals defined in hardwareInterface.cpp
extern uint32_t timeKeeper; //the master clock
extern bool resetTimer;
extern bool clockSlave;
extern bool changeToSlave;
extern bool changeToStandAlone;
extern outputStream textDisplay;

//FPGA-specific variables
//------------------------------------------
FPGADigitalIn dIn[NUMDIGINPORTS];
FPGADigitalOut dOut[NUMDIGOUTPORTS];
FPGAAnalogIn aIn[NUMANINPORTS];
FPGAAnalogOut aOut[NUMANOUTPORTS];

//Circular buffer for serial input
uint8_t serialInBuffer[SERIALINPUTBUFFERSIZE];
uint16_t serialInBufferReadLocation = 0;
uint16_t serialInBufferWriteLocation = 0;
uint64_t numSerialInBytesWritten = 0;
uint64_t numSerialInBytesRead = 0;
//--------------------------------------


volatile struct fifo txfifo = { 0, 0, 0, { 0 } };
volatile struct fifo rxfifo = { 0, 0, 0, { 0 } };

//Buffer for shortcut trigger commands
uint8_t shortcutInBuffer[5];
int shortcutBufferPos = 0;
//Buffer for shortcut triggers
uint16_t pendingTriggers[PENDINGTRIGGERBUFFERSIZE];
uint8_t numPendingTriggers;

int maxAnalogOut = 4981;

//-----------------------------------------
uint8_t UART_In_Use = 0; //either 0 for USB or 1 for MCU

FATFS Fatfs; // for file system

uint32_t millis = 0, old_millis = 0;

// Polling uart rx routine
uint8_t getByte(void)
{
  uint8_t c;
  // wait if RX fifo data
  while (!(UART0_UCR & UART_UCR_RX_DATA));
  c = UART0_UDR;
  return c;
}


// Polling uart tx routine
void sendByte(uint8_t c) {

  if (UART_In_Use == 0) {
    //USB communication
    // wait if TX fifo full
    while ((UART0_UCR & UART_UCR_TX_FULL));
    UART0_UDR = c;
  } else if (UART_In_Use == 1) {
      //MCU communication
      // wait if TX fifo full
      while ((UART1_UCR & UART_UCR_TX_FULL));
      UART1_UDR = c;

  }
}

void printString(char *line) {
  while (*line)
    sendByte(*line++);
}

uint8_t inbyte(void)
{
  uint8_t c;
  uint16_t i;
  while (rxfifo.count == 0);
  i = rxfifo.idx_r;
  c = rxfifo.buff[i++];
  if(i >= sizeof(rxfifo.buff))
    i = 0;
  rxfifo.idx_r = i;
  // Any operations on count must be done with interrupts disabled
  disable_interrupts();
  rxfifo.count--;
  enable_interrupts();
  return c;
}

void outbyte (uint8_t c) {
  uint16_t i;
  i = txfifo.idx_w;
  while (txfifo.count >= sizeof(txfifo.buff));
  txfifo.buff[i++] = c;
  // Any operations on count must be done with interrupts disabled
  disable_interrupts();
  txfifo.count++;
  enable_interrupts();
  // make sure TX interrupt is enabled
  if (UART_In_Use == 0) {
    UART0_UCR |= UART_UCR_TX_IE;
  } else if (UART_In_Use == 1) {
    UART1_UCR |= UART_UCR_TX_IE;
  }
  if(i >= sizeof(txfifo.buff))
    i = 0;
  txfifo.idx_w = i;
}

//This is the FPGA-specific callback for the clock increment timer----------------
void FPGATimerCallback (void) {
    if (TIMESTAMP > 0) {
        //If we are getting time from the MCU, mirror it with 1ms resolution
        timeKeeper = TIMESTAMP/30;
    } else {
        //increment stand-alone clock
        timeKeeper++;

        if (resetTimer) {
            timeKeeper = 0;
            resetTimer = false;
        }
    }
}

void printHexByte(uint8_t data)
{
  uint8_t nybble;
  sendByte('0');
  sendByte('x');
  nybble = (data >> 4) + 0x30;;
  if (nybble > 0x39)
    nybble += 7;
  sendByte(nybble);
  nybble = (data & 0x0f) + 0x30;;
  if (nybble > 0x39)
    nybble += 7;
  sendByte(nybble);
}

void printHexWord(uint32_t data)
{
  uint8_t nybble;
  sendByte('0');
  sendByte('x');
  nybble = ((data >> 28) & 0x0f) + 0x30;;
  if (nybble > 0x39)
    nybble += 7;
  sendByte(nybble);
  nybble = ((data >> 24) & 0x0f) + 0x30;;
  if (nybble > 0x39)
    nybble += 7;
  sendByte(nybble);
  nybble = ((data >> 20) & 0x0f) + 0x30;;
  if (nybble > 0x39)
    nybble += 7;
  sendByte(nybble);
  nybble = ((data >> 16) & 0x0f) + 0x30;;
  if (nybble > 0x39)
    nybble += 7;
  sendByte(nybble);
  nybble = ((data >> 12) & 0x0f) + 0x30;;
  if (nybble > 0x39)
    nybble += 7;
  sendByte(nybble);
  nybble = ((data >> 8) & 0x0f) + 0x30;;
  if (nybble > 0x39)
    nybble += 7;
  sendByte(nybble);
  nybble = ((data >> 4) & 0x0f) + 0x30;;
  if (nybble > 0x39)
    nybble += 7;
  sendByte(nybble);
  nybble = ((data >> 0) & 0x0f) + 0x30;;
  if (nybble > 0x39)
    nybble += 7;
  sendByte(nybble);
}

void msleep(unsigned int ms)
{
    TMR_DELAY = ms * 1000;
    while ((TMR_DCR & TMR_DCR_DLY_TO) == 0);
}

void usleep(unsigned int us)
{
    TMR_DELAY = us;
    while ((TMR_DCR & TMR_DCR_DLY_TO) == 0);
}

unsigned long get_ms()
{
  unsigned int millis = TMR_MILLIS;
  return millis;
}

volatile BYTE rtcYear = 2013-1980, rtcMon = 7, rtcMday = 6, rtcHour, rtcMin, rtcSec;

DWORD get_fattime (void)
{
    DWORD tmr;

    /* Pack date and time into a DWORD variable */
    tmr =	  (((DWORD)rtcYear - 80) << 25)
            | ((DWORD)rtcMon << 21)
            | ((DWORD)rtcMday << 16)
            | (WORD)(rtcHour << 11)
            | (WORD)(rtcMin << 5)
            | (WORD)(rtcSec >> 1);

    return tmr;
}

int badInterrupts = 0;

void myISR( void ) __attribute__ ((interrupt_handler));

//----------------------------------------------------
// INTERRUPT HANDLER FUNCTION
//----------------------------------------------------
void myISR( void )
{
  uint32_t status;
  // Check if timer interrupt bit set in interrupt controller
  if (INTC_IPR & TMR_INT) {
    // Yes, timer interrupt
      status = TMR_DCR;
    if (status & TMR_DCR_MS_IF) {
      // millisec interrupt
      TMR_DCR = (status & 0x7ff) | TMR_DCR_MS_IF; // clear interrupt flag
      millis++;
      // do other things needed every ms
      FPGATimerCallback();
    } else if (status & TMR_DCR_DLY_IF) {
      // delay interrupt
      TMR_DCR = (status & 0x7ff) | TMR_DCR_DLY_IF; // clear interrupt flag
      // do stuff in response to the delay interrupt
    }
    // Clear timer interrupt in interrupt controller
      INTC_IAR = TMR_INT;
  }
  else
  // Check if UART0 interrupt bit is set in interrupt controller
  if (INTC_IPR & UART0_INT) {
    // Yes, UART0 interrupt
    status = UART0_UCR;
    // First check RX interrupt
    while ((status & (UART_UCR_RX_DATA | UART_UCR_RX_IE)) == (UART_UCR_RX_DATA | UART_UCR_RX_IE)) {
      // uart0 rx interrupt
      UART_In_Use = 0; //Automatically switch to this UART for output
      uint8_t c = UART0_UDR;

      //put the char in the receive buffer
      serialInBuffer[serialInBufferWriteLocation] = c;
      serialInBufferWriteLocation = ((serialInBufferWriteLocation+1) % SERIALINPUTBUFFERSIZE);
      numSerialInBytesWritten++;

      status = UART0_UCR;

    }
    // Next check TX interrupt
    if ((status & (UART_UCR_TX_HALF | UART_UCR_TX_IE)) == UART_UCR_TX_IE) {
        uint16_t n, i;
        n = txfifo.count;
        if (n) {
            txfifo.count = --n;
            i = txfifo.idx_r;
            UART0_UDR = txfifo.buff[i++];
            if(i >= sizeof(txfifo.buff))
                i = 0;
            txfifo.idx_r = i;
        }
        if(n == 0)
            UART0_UCR = UART0_UCR & ~UART_UCR_TX_IE;
    }
    // place code for tx interrupt if needed here
    // Clear UART interrupt in interrupt controller
    INTC_IAR = UART0_INT;
  }
  else
  // Check if UART1 interrupt bit is set in interrupt controller (info from MCU)
  if (INTC_IPR & UART1_INT) {
    // Yes, UART1 interrupt
    status = UART1_UCR;
    while ((status & (UART_UCR_RX_DATA | UART_UCR_RX_IE)) == (UART_UCR_RX_DATA | UART_UCR_RX_IE)) {
      // uart1 rx interrupt
      UART_In_Use = 1; //Automatically switch to this UART for output
      uint8_t c = UART1_UDR;

      serialInBuffer[serialInBufferWriteLocation] = c;
      serialInBufferWriteLocation = ((serialInBufferWriteLocation+1) % SERIALINPUTBUFFERSIZE);
      numSerialInBytesWritten++;

      // do something with the received char, like send it out via UART0
      //UART0_UDR = c;
      //UART1_UDR = c;
      status = UART1_UCR;
    }
    // Next check TX interrupt
    if ((status & (UART_UCR_TX_HALF | UART_UCR_TX_IE)) == UART_UCR_TX_IE) {
        uint16_t n, i;
        n = txfifo.count;
        if (n) {
            txfifo.count = --n;
            i = txfifo.idx_r;
            UART1_UDR = txfifo.buff[i++];
            if(i >= sizeof(txfifo.buff))
                i = 0;
            txfifo.idx_r = i;
        }
        if(n == 0)
            UART1_UCR = UART1_UCR & ~UART_UCR_TX_IE;
    }
    // place code for tx interrupt if needed here
    // Clear UART interrupt in interrupt controller
      INTC_IAR = UART1_INT;
  }
  else
  // Check if UART2 interrupt bit is set in interrupt controller (shortcut commands from MCU)
  if (INTC_IPR & UART2_INT) {
    // Yes, UART2 interrupt
    status = UART2_UCR;

    while ((status & (UART_UCR_RX_DATA | UART_UCR_RX_IE)) == (UART_UCR_RX_DATA | UART_UCR_RX_IE)) {
      // uart2 rx interrupt
      uint8_t c = UART2_UDR;

      shortcutInBuffer[shortcutBufferPos] = c;
      shortcutBufferPos = (shortcutBufferPos+1) % 2;

      //convert the two bytes to an unsigned short
      if (shortcutBufferPos == 0) {
          uint16_t triggerNum = *(uint16_t*)shortcutInBuffer;
          //add the trigger number to the buffer (will be executed on the next main loop)
          if ((triggerNum > 0)&&(numPendingTriggers < PENDINGTRIGGERBUFFERSIZE)) {
              pendingTriggers[numPendingTriggers] = triggerNum-1; //convert from 1-based notation
              numPendingTriggers++;

          }
      }

      // do something with the received char, like send it out via UART0
      //UART0_UDR = c;

      status = UART2_UCR;
    }
    // place code for tx interrupt if needed here
    // Clear UART interrupt in interrupt controller
      INTC_IAR = UART2_INT;
  }

  else
  // Check if digital I/O edge interrupt bit is set in interrupt controller
  if (INTC_IPR & DIO_INT) {

    // Yes, DIO edge interrupt
    // First check rising edge bits
    uint32_t edge = (DIO_RINT_FLAG & DIO_RINT_MASK);
    if (edge) {
      // process rising edge bits here
      //TODO:  call interrupt_up_callback()
      //read the edge state one by one
      for (int i=0;i<NUMDIGINPORTS;i++) {
          if (edge & (1 << i)) {
            dIn[i].interrupt_up_callback();
          }
      }
      DIO_RINT_FLAG = edge; // clear edge bits. Writing a 1 to bit clears it.
    }
    // next check falling edge bits
    edge = (DIO_FINT_FLAG & DIO_FINT_MASK);
    if (edge) {
      // process falling edge bits here
      //TODO:  call interrupt_down_callback()
        //read the edge state one by one
        for (int i=0;i<NUMDIGINPORTS;i++) {
            if (edge & (1 << i)) {
              dIn[i].interrupt_down_callback();
            }
        }
      DIO_FINT_FLAG = edge; // clear edge bits
    }
    // Clear DIO interrupt in interrupt controller
      INTC_IAR = DIO_INT;

  }
  else
  // Check if SPI0 interrupt bit is set in interrupt controller
  if (INTC_IPR & SPI0_INT) {
    // Handle SPI0 interrupt here

    // Clear SPI0 interrupt in interrupt controller
      INTC_IAR = SPI0_INT;
  }

  else {
      badInterrupts++;
      if (badInterrupts > 1000) {
          UART0_UDR = 'x';
      }
  }
}

//------------------------------------------------------------


FPGASystem::FPGASystem()
    //Define the clock reset and increment pins here
    //:clockResetInt(p5),
    //clockExternalIncrement(p8)
    {

    //Attach a reset function to the reset pin interupt
    //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------
    //clockResetInt.rise(this, &FPGASystem::externalClockReset);
    //clockResetInt.mode(PullDown);
    //clockExternalIncrement.mode(PullDown);
    //------------------------------------------------------

}

void FPGASystem::timerinit() {

      // Enable instruction and data cache
      enable_icache();
      enable_dcache();

      // Init timer: clocks per microsecond, timer enable, ms interrupt enable, delay interrupt enable
      TMR_DCR = (F_CPU/1000000) | TMR_DCR_ENABLE | TMR_DCR_MS_IE | TMR_DCR_DLY_IE;

      // Init UART0 (from computer)
      UART0_UBRR = 53; // 115200  baud
      UART0_UCR = UART_UCR_ENABLE | UART_UCR_RX_IE; // uart enable, uart rx interrupt enable

      // Init UART1 (from MCU)
      UART1_UBRR = 53; // 115200  baud

      UART1_UCR = UART_UCR_ENABLE | UART_UCR_RX_IE; // uart enable, uart rx interrupt enable

      // Init UART2 (shortcut control of IO, from MCU)
      UART2_UBRR = 53; // 115200  baud
      //UART2_UBRR = 7; // 781250  baud
      UART2_UCR = UART_UCR_ENABLE | UART_UCR_RX_IE; // uart enable, uart rx interrupt enable

      // Init SD-card SPI controller
      SPI0_PORTSET = 1 << SPI_SS; // make CS output
      SPI0_DIRSET = 1 << SPI_SS;  // set CS high

      // DIO
      DIO_FINT_MASK = 0x00000000; // disable falling int on all DIO inputs
      DIO_RINT_MASK = 0x00000000; // disable rising edge on all DIO inputs
      DIO_DCR = DIO_DCR_IE;       // enable DIO edge interrupts

      if (disk_initialize(0)) {
            printString("ERROR: Unable to initialize SD card!\r\n");
        } else {
        if (f_mount(&Fatfs, "", 0)) {
          printString("ERROR: Unable to mount file system!\r\n");
        } else {
          printString("SD-card succesfully mounted!\n");
        }
      }

      // Clear any pending interrupts
      TMR_DCR |= (TMR_DCR_MS_IF | TMR_DCR_DLY_IF); // clear any pending timer interrupt
      DIO_RINT_FLAG = 0xffffffff; // clear any pending rising edge interrupts
      DIO_FINT_FLAG = 0xffffffff; // clear any pending falling edge interrupts

      // Init interrupt controller
      INTC_IER = TMR_INT | UART0_INT | UART1_INT | UART2_INT | DIO_INT | SPI0_INT;

      INTC_MER = 0x03; // enable interrupt controller

      // Enable Microblaze interrupts
      enable_interrupts();

      for (int i=0; i < NUMDIGINPORTS; i++) {
          dIn[i].init(i);
      }
      for (int i=0; i < NUMDIGOUTPORTS; i++) {
          dOut[i].init(i);
      }
      for (int i=0; i < NUMANINPORTS; i++) {
          aIn[i].init(i);
      }
      for (int i=0; i < NUMANOUTPORTS; i++) {
          aOut[i].init(i);
      }

}

void FPGASystem::pauseInterrupts() {
    disable_interrupts();
}

void FPGASystem::reset() {
    shortcutBufferPos = 0; //Reset the shortcut buffer
}

void FPGASystem::setMaxAnalogOut(int value) {
    maxAnalogOut = value;
}

void FPGASystem::resumeInterrupts() {
    enable_interrupts();
}

int FPGASystem::getPendingFunctionTriggers(uint16_t* bufferPtr) {

    int numPendingTriggers_copy;
    disable_interrupts();

    for (uint8_t i=0; i < numPendingTriggers; i++) {
        *bufferPtr = pendingTriggers[i];
        bufferPtr++;
    }
    numPendingTriggers_copy = numPendingTriggers;
    numPendingTriggers = 0; //Reset the shortcup trigger buffer
    enable_interrupts();
    return numPendingTriggers_copy;
}

uint32_t FPGASystem::getDigitalOutputChangeFlags() {

}

uint32_t FPGASystem::getDigitalInputChangeFlags() {

}

//----------------------------------------------

sDigitalOut* FPGASystem::getDigitalOutPtr(int portNum){
    if (portNum < NUMDIGOUTPORTS) {
        return &dOut[portNum];
    } else {
        return NULL;
    }
}

sDigitalIn* FPGASystem::getDigitalInPtr(int portNum) {
    if (portNum < NUMDIGINPORTS) {
        return &dIn[portNum];
    } else {
        return NULL;
    }
}

sAnalogOut* FPGASystem::getAnalogOutPtr(int portNum){
    if (portNum < NUMANOUTPORTS) {
        return &aOut[portNum];
    } else {
        return NULL;
    }
}

sAnalogIn* FPGASystem::getAnalogInPtr(int portNum) {
    if (portNum < NUMANINPORTS) {
        return &aIn[portNum];
    } else {
        return NULL;
    }
}

sSound* FPGASystem::createNewSoundAction() {
    FPGASound *tmpSound = new FPGASound();
    return tmpSound;
}


void FPGASystem::externalClockReset() {
    if (timeKeeper > 100) {


        //FPGA-specific code to reset timer here------


        //----------------------------------------------


        immediateClockReset();
    }
}

void FPGASystem::setStandAloneClock() {
    timerinit();

    //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------
    //clockExternalIncrement.rise(NULL); //remove the callback to the external interrupt
    //------------------------------------------------------

    clockSlave = false;
    changeToSlave = false;
    changeToStandAlone = false;
}

void FPGASystem::setSlaveClock() {

    //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------
    //NVIC_DisableIRQ(TIMER0_IRQn); // Disable the timer interrupt
    //clockExternalIncrement.rise(this, &FPGASystem::incrementClock); //enable the external pin interrupt
    //----------------------------------------------------
    clockSlave = true;
    changeToSlave = false;
    changeToStandAlone = false;
}

void FPGASystem::incrementClock() {
    timeKeeper++;
    //Clock resets happen upon update so we dont get a partial first ms
    if (resetTimer) {
        timeKeeper = 0;
        resetTimer = false;
    }
}

void FPGASystem::mainLoopToDo() {


}

//-----------------------------------------------------

FPGASound::FPGASound() {

}

void FPGASound::execute() {
    if (reset) {
        //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------
        //code to reset sound system (if needed)
        //sWav.reset();
        //----------------------------------------------------
    } else if (!play) {
        //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------
        //code to stop playback here
        //sWav.stopTrack();
        //----------------------------------------------
    } else {
        if (volume > -1) {
            //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------
            //Code to change volume
            //sWav.volume(volume);
            //------------------------------------------
        } else if (volumePtr != NULL) {
            //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------
            //code to change volume
            //sWav.volume(*volumePtr);
            //---------------------------------------------
        }

        if (fileNameExists) {
            //NEEDS FPGA-SPECIFIC IMPLEMENTATION-------------------

            //code to first stop playback, then play the file ("myFile" means there is a "myFile.wav" on the sd card)
            //sWav.stopTrack();
            //sWav.playTrackName(fileName);
            //------------------------------------------------
        }
    }
}


//-----------------------------------------------------
FPGAAnalogOut::FPGAAnalogOut() {

}

void FPGAAnalogOut::init(int pin) {
    pinNumber = pin;

    //---------------------------------------------------
}

int FPGAAnalogOut::read() {

    uint16_t data;

    switch (pinNumber) {
        case 0:
            data = AOUT_CH0;
            break;
        case 1:
            data = AOUT_CH1;
            break;
        case 2:
            data = AOUT_CH2;
            break;
        case 3:
            data = AOUT_CH3;
            break;
    }

    data = data*((float)maxAnalogOut/65535.0); //0-5V range
    return data;

}

void FPGAAnalogOut::write(int value) {

    uint16_t data;
    if (value > maxAnalogOut) {
        data = maxAnalogOut;
    } else if (value < 0) {
        data = 0;
    } else {
        data = value;
    }
    data = data*(65535.0/(float)maxAnalogOut); //0-5V range

    switch (pinNumber) {
        case 0:
            AOUT_CH0 = data;
            break;
        case 1:
            AOUT_CH1 = data;
            break;
        case 2:
            AOUT_CH2 = data;
            break;
        case 3:
            AOUT_CH3 = data;
            break;
    }


}
//--------------------------------------------------------

FPGAAnalogIn::FPGAAnalogIn() {

}

void FPGAAnalogIn::init(int pin) {

    pinNumber = pin;

}

int FPGAAnalogIn::read() {

    int16_t data;

    switch (pinNumber) {
        case 0:
            data = AIN_CH0;
            break;
        case 1:
            data = AIN_CH1;
            break;
        case 2:
            data = AIN_CH2;
            break;
        case 3:
            data = AIN_CH3;
            break;
        case 4:
            data = AIN_CH4;
            break;
        case 5:
            data = AIN_CH5;
            break;
        case 6:
            data = AIN_CH6;
            break;
        case 7:
            data = AIN_CH7;
            break;
    }

    data = data*(10000.0/32767.0); //+/- 10V range
    return data;

    //----------------------------------------------------
}









//-----------------------------------------------------
FPGADigitalOut::FPGADigitalOut() {

}

void FPGADigitalOut::init(int pin) {
    pinNumber = pin;

    //---------------------------------------------------
}

int FPGADigitalOut::read() {

    //read the pin state
    //uint32_t data = DIO_PINS_OUT; // read output pins (actual hardware state)
    uint32_t data = DIO_DOUT; // read output pins (register state)
    return (int)((data & (1 << pinNumber)) >> pinNumber);

}

void FPGADigitalOut::write(int value) {

    //code to write a value to the digital pin (1 or 0)
    //uint32_t data = DIO_PINS_OUT; // read output pins (actual hardware state)
    uint32_t data = DIO_DOUT; // read output pins (register state)

    if (value <= 0) {
        //set bit to 0
        data &= ~(1 << pinNumber);
    } else {
        //set bit to 1
        data |= 1 << pinNumber;
    }

    DIO_DOUT = data; // set dio output

}
//--------------------------------------------------------

FPGADigitalIn::FPGADigitalIn() {

}

void FPGADigitalIn::init(int pin) {

    //create an input pin object, and an interrupt object for the same pin
    pinNumber = pin;

    //Here we enable both falling and rising edge interrupts for the pin, using
    //a read, modify, write method
    uint32_t fallingMask = DIO_FINT_MASK; // which pins are enabled for falling interrupt
    uint32_t risingMask = DIO_RINT_MASK; // which pins are enabled for rising interrupt

    fallingMask |= 1 << pinNumber;
    risingMask |= 1 << pinNumber;

    DIO_FINT_MASK = fallingMask; // enable falling int on pin
    DIO_RINT_MASK = risingMask; // enable rising edge on pin

    //inpin = new DigitalIn(inPins[pin]);
    //inpin_interrupt = new InterruptIn(inPins[pin]);
    //inpin->mode(PullDown);

    //code to set up callbacks for the port interrupts
    //inpin_interrupt->rise(this, &FPGADigitalIn::interrupt_up_callback);
    //inpin_interrupt->fall(this, &FPGADigitalIn::interrupt_down_callback);
    //---------------------------------------------------------
}

int FPGADigitalIn::read() {

    //code to read to digital input pin
    uint32_t data = DIO_PINS_IN; // read input pins
    return (int)((data & (1 << pinNumber)) >> pinNumber);

    //----------------------------------------------------
}

void FPGADigitalIn::interrupt_up_callback() {
    addStateChange(1, timeKeeper);
}

void FPGADigitalIn::interrupt_down_callback() {
    addStateChange(0, timeKeeper);
}

//----------------------------------------------------------
FPGASerialPort::FPGASerialPort() {

}

void FPGASerialPort::init() {


    //initialize serial communication for standalone operation (115200 baud)
    //or a method of talking through the MCU in slave mode
    // Init UART0
    //UART0_UBRR = 53; // 115200  baud
    //UART0_UCR = UART_UCR_ENABLE | UART_UCR_RX_IE; // uart enable, uart rx interrupt enable


}

bool FPGASerialPort::readable() {

    //determine whether or not there is anything to read
    //we must disable interrupts for this to be safe
    uint64_t bytesAvailable;
    disable_interrupts();
    bytesAvailable = numSerialInBytesWritten-numSerialInBytesRead;
    enable_interrupts();

    if (bytesAvailable > 0) {
        return true;
    } else {
        return false;
    }
}

char FPGASerialPort::readChar() {   
    //get the next character in the input buffer
    char outchar = serialInBuffer[serialInBufferReadLocation];
    serialInBufferReadLocation = ((serialInBufferReadLocation+1) % SERIALINPUTBUFFERSIZE);
    numSerialInBytesRead++;

    return outchar;
}

int FPGASerialPort::requestToWriteString(char *s, int numBytesRequested) {
    //request to print a string to the serial output buffer
    //function returns the number of chars actually accepted for output
    int numBytesAccepted = 0;
    while (numBytesAccepted < numBytesRequested) {
        outbyte(*(s+numBytesAccepted));
        numBytesAccepted++;
    }

    return numBytesAccepted;
}

void FPGASerialPort::writeChar(char s) {
    /*
    uint16_t i;
    i = txfifo.idx_w;
    while (txfifo.count >= sizeof(txfifo.buff));
    txfifo.buff[i++] = s;
    // Any operations on count must be done with interrupts disabled
    disable_interrupts();
    txfifo.count++;
    enable_interrupts();
    // make sure TX interrupt is enabled
    if (UART_In_Use == 0) {
      UART0_UCR |= UART_UCR_TX_IE;
    } else if (UART_In_Use == 1) {
      UART1_UCR |= UART_UCR_TX_IE;
    }
    if(i >= sizeof(txfifo.buff))
      i = 0;
    txfifo.idx_w = i;
    */


    //code to print a single character to the output buffer
    if (UART_In_Use == 0) {
      //USB communication
      // wait if TX fifo full
      while ((UART0_UCR & UART_UCR_TX_FULL));
      UART0_UDR = s;
    } else if (UART_In_Use == 1) {
        //MCU communication
        // wait if TX fifo full
        while ((UART1_UCR & UART_UCR_TX_FULL));
        UART1_UDR = s;

    }
}
