#ifndef FPGAINTERFACE_H
#define FPGAINTERFACE_H

#include "hardwareInterface.h"
#include <stdint.h>
#include <string.h>
#include <string>
#include <vector>
#include <queue>
#include <stdio.h>
#include "registers.h"
#include "utils.h"
#include "ff.h"
#include "diskio.h"

#define SERIALINPUTBUFFERSIZE 4096


#define NUMDIGINPORTS 32 //the number of ports available on this hardware
#define NUMDIGOUTPORTS 32
#define NUMANINPORTS 8
#define NUMANOUTPORTS 4

#define NUMEVENTS 100
#define NUMCONDITIONS 300
#define NUMINTCOMPARE 300
#define NUMACTIONS 300
#define NUMPORTMESSAGES 300
#define NUMINTOPERATIONS 300
#define NUMDISPLAYACTIONS 50
#define NUMTRIGGERACTIONS 100
#define NUMFUNCTIONS 50
#define INPUTCHARBUFFERSIZE 6000

struct fifo {
  uint16_t idx_w;
  uint16_t idx_r;
  uint16_t count;
  uint8_t buff[1024];
};


class FPGAAnalogOut : public sAnalogOut
{
public:
    FPGAAnalogOut();

    void init(int pin);
    void write(int value);
    int read();

private:
    //define the hardware output pin
    uint8_t pinNumber;

};

class FPGAAnalogIn : public sAnalogIn
{
public:
    FPGAAnalogIn();

    void init(int pin);
    int read();

protected:

private:
    uint8_t pinNumber;


};

class FPGADigitalOut : public sDigitalOut
{
public:
    FPGADigitalOut();

    void init(int pin);
    void write(int value);
    int read();

private:
    //define the hardware output pin
    uint8_t pinNumber;

};

class FPGADigitalIn : public sDigitalIn
{
public:
    FPGADigitalIn();

    void init(int pin);
    int read();
    void interrupt_up_callback();
    void interrupt_down_callback();
protected:

private:
    uint8_t pinNumber;
    //define the harware input pin
    //DigitalIn *inpin;
    //InterruptIn *inpin_interrupt;

};

class FPGASerialPort : public sSerialPort
{
public:
    FPGASerialPort();

    void init();
    bool readable();
    char readChar();
    void writeChar(char s);
    int  requestToWriteString(char *s, int numBytes);
    //Serial *serialToPC;

private:



};

class FPGASound: public sSound
{
public:
    FPGASound();
    void execute();

private:

};

class FPGASystem: public sSystem
{
public:
    FPGASystem();
    void timerinit();
    void setStandAloneClock();
    void setSlaveClock();
    sDigitalOut* getDigitalOutPtr(int portNum);
    sDigitalIn* getDigitalInPtr(int portNum);
    sAnalogOut* getAnalogOutPtr(int portNum);
    sAnalogIn* getAnalogInPtr(int portNum);
    sSound* createNewSoundAction();
    void incrementClock();
    void externalClockReset(); //needs to reset harware timer before calling immediateClockReset();
    void mainLoopToDo();
    void pauseInterrupts();
    void resumeInterrupts();
    void reset();
    void setMaxAnalogOut(int value);
    int  getPendingFunctionTriggers(uint16_t *bufferPtr);
    uint32_t getDigitalOutputChangeFlags();
    uint32_t getDigitalInputChangeFlags();

protected:

    //Pins for clock syncing (one for reset, one for increment)
    //InterruptIn clockResetInt;
    //InterruptIn clockExternalIncrement;

private:
    //FPGADigitalIn dIn[NUMPORTS];
    //FPGADigitalOut dOut[NUMPORTS];
};


#endif // FPGAINTERFACE_H
