#ifndef HARDWARWEINTERFACE_H
#define HARDWARWEINTERFACE_H

#include <stdint.h>
#include <string>
#include <cstdlib>
#include <string.h>

#define PENDINGTRIGGERBUFFERSIZE 10

//#define NUMPORTS 8 //the number of ports available on the hardware

using namespace std;

//for debugging output
//#define DEBUGOUTPUT

//used to organize digital change events
struct changeEvent {
    uint32_t timeStamp;
    bool triggered;
};

class sDigitalOut
{
public:
    sDigitalOut();

    virtual void init(int pin) = 0;
    virtual void write(int value) = 0;
    virtual int read() = 0;
protected:

};

class sAnalogIn
{
public:
    sAnalogIn();

    virtual void init(int pin) = 0;
    virtual int read() = 0;


protected:


};

class sAnalogOut
{
public:
    sAnalogOut();

    virtual void init(int pin) = 0;
    virtual void write(int value) = 0;
    virtual int read() = 0;
protected:

};



class sDigitalIn
{
public:
    sDigitalIn();

    virtual void init(int pin) = 0;
    virtual int read() = 0;
    virtual void interrupt_up_callback() = 0;
    virtual void interrupt_down_callback() = 0;
    void setUpdate(bool);

    changeEvent lastUpEvent;
    changeEvent lastDownEvent;

protected:

    bool updating;
    changeEvent bufferedUpEvent;
    changeEvent bufferedDownEvent;
    void addStateChange(int newState, uint32_t timeStamp);

};

class sSerialPort
{
public:
    sSerialPort();

    virtual void init() = 0;
    virtual bool readable() = 0;
    virtual char readChar() = 0;
    virtual void writeChar(char s)= 0;
    virtual int  requestToWriteString(char *s, int numBytes) = 0;

protected:


};

class sSound
{
public:
    sSound();
    void setFile(string fileNameIn);
    void setVolume(int* volumeIn);
    void setVolume(int volumeIn);
    void setPlayback(bool playIn);
    void setReset();
    virtual void execute() = 0;

protected:
    char fileName[21];
    bool fileNameExists;
    int* volumePtr;
    int volume;
    bool play;
    bool reset;
};

class sSystem
{
public:
    sSystem();
    virtual void timerinit() = 0;
    virtual void setStandAloneClock() = 0;
    virtual void setSlaveClock() = 0;
    virtual sDigitalOut* getDigitalOutPtr(int portNum) = 0;
    virtual sDigitalIn* getDigitalInPtr(int portNum) = 0;
    virtual sAnalogOut* getAnalogOutPtr(int portNum) = 0;
    virtual sAnalogIn* getAnalogInPtr(int portNum) = 0;
    virtual sSound* createNewSoundAction() = 0;
    virtual void externalClockReset() = 0; //needs to reset harware timer before calling immediateClockReset();
    virtual void incrementClock() = 0;
    virtual void mainLoopToDo();
    virtual void pauseInterrupts();
    virtual void resumeInterrupts();
    virtual void reset();
    virtual void setMaxAnalogOut(int value);
    virtual int getPendingFunctionTriggers(uint16_t *bufferPtr); //Returns the number of pending shortcut triggers
    virtual uint32_t getDigitalOutputChangeFlags();
    virtual uint32_t getDigitalInputChangeFlags();
    void setPortUpdatesOn(int portNum);
    void setPortUpdatesOff(int portNum);
    bool* getIgnoreUpdates();



protected:

    //virtual void timerInterrupt() = 0;

    void immediateClockReset();   
    int currentDINPin;
    int currentDOUTPin;
    uint32_t currentDigitalOuputStates;
    uint32_t currentDigitalInputStates;
    bool ignorePortUpdates[32];




};


//Used to buffer output text. Used mainly for 'display' commands within the script,
//and alloed the reset of the block to execute quickly instead.  The text is then streamed out
//slowly via serial (one character per main loop execution). outputStream is a simple circular
//buffer that cannot be resized after initiation.
class outputStream {

public:
    outputStream(int bufferSizeIn);
    ~outputStream();
    void setSerial(sSerialPort *s);
    void send(const string &outputString);
    void send(const char* s);
    void debug(const char* s);
    outputStream &operator<<(const string &outputString);
    outputStream &operator<<(int outputNum);
    outputStream &operator<<(uint32_t outputNum);
    outputStream &operator<<(const char* s);
    char getNextChar();
    bool unsentData;
    void flush();

private:
    int readHead;
    int writeHead;
    int totalWriteHead;
    int totalReadHead;
    int bufferSize;
    char tmpOut;
    char* outputBuffer;
    sSerialPort* serialPtr;
};



#endif // HARDWARWEINTERFACE_H
