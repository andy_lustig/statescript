#ifndef MBEDINTERFACE_H
#define MBEDINTERFACE_H

#include "hardwareInterface.h"
#include "mbed.h"
#include <stdint.h>
#include "SMARTWAV.h"
#include <string.h>
#include <string>
#include <vector>
#include <queue>
#include <sstream>


//#define MBED_RF


//#define NUMPORTS 10 //the number of ports available on this hardware
#define NUMDIGINPORTS 8
#define NUMDIGOUTPORTS 8
#define NUMANINPORTS 1
#define NUMANOUTPORTS 1

#define NUMEVENTS 50
#define NUMCONDITIONS 150
#define NUMINTCOMPARE 150
#define NUMACTIONS 150
#define NUMPORTMESSAGES 150
#define NUMINTOPERATIONS 150
#define NUMDISPLAYACTIONS 30
#define NUMTRIGGERACTIONS 30
#define NUMFUNCTIONS 50
#define INPUTCHARBUFFERSIZE 3072



/*
class MBEDTimer : public sTimer
{
public:
    sTimer();
    virtual void init() = 0;
    virtual void timeout_callback() = 0;

};

class MBEDExternalSync : public sExternalSync
{
public:
    MBEDExternalSync();
    void init();
    void interrupt_callback();
    void reset_callback();

};*/

class MBEDAnalogOut : public sAnalogOut
{
public:
    MBEDAnalogOut();

    void init(int pin);
    void write(int value);
    int read();

private:
    //define the hardware output pin
    uint8_t pinNumber;
    AnalogOut *outpin;
    bool pinExists;

};

class MBEDAnalogIn : public sAnalogIn
{
public:
    MBEDAnalogIn();

    void init(int pin);
    int read();

protected:

private:
    uint8_t pinNumber;
    AnalogIn *inpin;
    bool pinExists;


};

class MBEDDigitalOut : public sDigitalOut
{
public:
    MBEDDigitalOut();

    void init(int pin);
    void write(int value);
    int read();

private:
    DigitalOut *outpin;
    bool pinExists;

};

class MBEDDigitalIn : public sDigitalIn
{
public:
    MBEDDigitalIn();

    void init(int pin);
    int read();
    void interrupt_up_callback();
    void interrupt_down_callback();
protected:



private:
    DigitalIn *inpin;
    InterruptIn *inpin_interrupt;
    bool pinExists;

};

class MBEDSerialPort : public sSerialPort
{
public:
    MBEDSerialPort();

    void init();
    bool readable();
    char readChar();
    void writeChar(char s);
    int  requestToWriteString(char *s, int numBytes);
    Serial *serialToPC;

private:
    //Serial communication


};

class MBEDSound: public sSound
{
public:
    MBEDSound();
    void execute();

private:

};

class MBEDSystem: public sSystem
{
public:
    MBEDSystem();
    void timerinit();
    void setStandAloneClock();
    void setSlaveClock();
    sDigitalOut* getDigitalOutPtr(int portNum);
    sDigitalIn* getDigitalInPtr(int portNum);
    sAnalogOut* getAnalogOutPtr(int portNum);
    sAnalogIn* getAnalogInPtr(int portNum);
    sSound* createNewSoundAction();
    void pauseInterrupts();
    void resumeInterrupts();
    void incrementClock();
    void externalClockReset(); //needs to reset harware timer before calling immediateClockReset();
    void mainLoopToDo();

protected:

    //Pins for clock syncing
    InterruptIn clockResetInt;
    InterruptIn clockExternalIncrement;

private:
    MBEDDigitalIn dIn[NUMDIGINPORTS];
    MBEDDigitalOut dOut[NUMDIGOUTPORTS];
    MBEDAnalogIn aIn[NUMANINPORTS];
    MBEDAnalogOut aOut[NUMANOUTPORTS];
};

#endif // MBEDINTERFACE_H
