#include "registers.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
void outbyte(char c); 

#ifdef __cplusplus
}
#endif 

void outbyte(char c) {
  // wait if TX fifo full
  while ((UART0_UCR & UART_UCR_TX_FULL));
  UART0_UDR = c;
}
