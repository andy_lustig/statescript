// register map
#define INTC_ISR       (*(volatile uint32_t *)(0x41200000)) /* Interrupt Status Register */
#define INTC_IPR       (*(volatile uint32_t *)(0x41200004)) /* Interrupt Pending Register */
#define INTC_IER       (*(volatile uint32_t *)(0x41200008)) /* Interrupt Enable Register */
#define INTC_IAR       (*(volatile uint32_t *)(0x4120000c)) /* Interrupt Acknowledge Register */
#define INTC_SIE       (*(volatile uint32_t *)(0x41200010)) /* Set Interrupt Enable Register */
#define INTC_CIE       (*(volatile uint32_t *)(0x41200014)) /* Clear Interrupt Enable Register */
#define INTC_IVR       (*(volatile uint32_t *)(0x41200018)) /* Interrupt Vector Register */
#define INTC_MER       (*(volatile uint32_t *)(0x4120001c)) /* Master Enable Register */

#define UART0_UDR      (*(volatile  uint8_t *)(0x42000000))
#define UART0_UCR      (*(volatile uint16_t *)(0x42000004))
#define UART0_UBRR     (*(volatile uint16_t *)(0x42000008))
#define UART0_INT      0x000002

#define UART1_UDR      (*(volatile  uint8_t *)(0x42100000))
#define UART1_UCR      (*(volatile uint16_t *)(0x42100004))
#define UART1_UBRR     (*(volatile uint16_t *)(0x42100008))
#define UART1_INT      0x000004

#define UART2_UDR      (*(volatile  uint8_t *)(0x42180000))
#define UART2_UCR      (*(volatile uint16_t *)(0x42180004))
#define UART2_UBRR     (*(volatile uint16_t *)(0x42180008))
#define UART2_INT      0x000008

#define TMR_DCR        (*(volatile uint16_t *)(0x42200000))
#define TMR_DELAY      (*(volatile uint32_t *)(0x42200004))
#define TMR_MICROS     (*(volatile uint32_t *)(0x42200008))
#define TMR_MILLIS     (*(volatile uint32_t *)(0x4220000c))
#define TMR_INT        0x000001

#define DIO_DCR        (*(volatile  uint8_t *)(0x42300000))
#define DIO_PINS_IN    (*(volatile uint32_t *)(0x42300004))
#define DIO_PINS_OUT   (*(volatile uint32_t *)(0x42300008))
#define DIO_DOUT       (*(volatile uint32_t *)(0x4230000c))
#define DIO_DOUT_SET   (*(volatile uint32_t *)(0x42300010))
#define DIO_DOUT_CLR   (*(volatile uint32_t *)(0x42300014))
#define DIO_RINT_MASK  (*(volatile uint32_t *)(0x42300018))
#define DIO_FINT_MASK  (*(volatile uint32_t *)(0x4230001c))
#define DIO_RINT_FLAG  (*(volatile uint32_t *)(0x42300020))
#define DIO_FINT_FLAG  (*(volatile uint32_t *)(0x42300024))
#define DIO_INT        0x000010

#define AOUT_CH0       (*(volatile uint16_t *)(0x42400000))
#define AOUT_CH1       (*(volatile uint16_t *)(0x42400004))
#define AOUT_CH2       (*(volatile uint16_t *)(0x42400008))
#define AOUT_CH3       (*(volatile uint16_t *)(0x4240000c))

#define AIN_CH0        (*(volatile  int16_t *)(0x42500000))
#define AIN_CH1        (*(volatile  int16_t *)(0x42500004))
#define AIN_CH2        (*(volatile  int16_t *)(0x42500008))
#define AIN_CH3        (*(volatile  int16_t *)(0x4250000c))
#define AIN_CH4        (*(volatile  int16_t *)(0x42500010))
#define AIN_CH5        (*(volatile  int16_t *)(0x42500014))
#define AIN_CH6        (*(volatile  int16_t *)(0x42500018))
#define AIN_CH7        (*(volatile  int16_t *)(0x4250001c))

#define SPI0_PORT      (*(volatile uint16_t *)(0x43000000))
#define SPI0_PORTSET   (*(volatile uint16_t *)(0x43000004))
#define SPI0_PORTCLR   (*(volatile uint16_t *)(0x43000008))
#define SPI0_PORTTGL   (*(volatile uint16_t *)(0x4300000c))
#define SPI0_DIR       (*(volatile uint16_t *)(0x43000010))
#define SPI0_DIRSET    (*(volatile uint16_t *)(0x43000014))
#define SPI0_DIRCLR    (*(volatile uint16_t *)(0x43000018))
#define SPI0_PIN       (*(volatile uint16_t *)(0x4300001c))
#define SPI0_SPCR      (*(volatile  uint8_t *)(0x43000020))
#define SPI0_SPSR      (*(volatile  uint8_t *)(0x43000024))
#define SPI0_SPDR      (*(volatile  uint8_t *)(0x43000028))
#define SPI0_INT       0x000020

#define TIMESTAMP      (*(volatile uint32_t *)(0x44000000))

// UART UCR register bit definitions
#define UART_UCR_ENABLE    0x80
#define UART_UCR_RX_IE     0x40
#define UART_UCR_TX_IE     0x20
#define UART_UCR_RX_DATA   0x10
#define UART_UCR_RX_FULL   0x08
#define UART_UCR_TX_DATA   0x04
#define UART_UCR_TX_FULL   0x02
#define UART_UCR_TX_HALF   0x01

// Timer DCR register bit definitions
#define TMR_DCR_ENABLE     0x0100
#define TMR_DCR_MS_IE      0x0200
#define TMR_DCR_DLY_IE     0x0400
#define TMR_DCR_DLY_TO     0x0800
#define TMR_DCR_MS_IF      0x1000
#define TMR_DCR_DLY_IF     0x2000

// DIO DCR register bit definitions
#define DIO_DCR_IE         0x01
#define DIO_DCR_IF         0x02

// SPI I/O bit definitions
#define SPI_MISO           0
#define SPI_MOSI           1
#define SPI_SCK            2
#define SPI_SS             3

// SPI SPCR register bit definitions
#define SPIE               7
#define SPE                6
#define DORD               5
#define MSTR               4
#define CPOL               3
#define CPHA               2
#define SPR1               1
#define SPR0               0
  
// SPI SPSR register bit definitions
#define SPIF               7
#define SPI2X              0


// Processor clock rate (in Hz)
#define F_CPU              100000000






