TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    behave.cpp \
    hardwareInterface.cpp \
    mbedInterface.cpp \
    fpgaInterface.cpp \
    debugInterface.cpp

HEADERS += \
    behave.h \
    hardwareInterface.h \
    mbedInterface.h \
    fpgaInterface.h \
    registers.h \
    debugInterface.h

